﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

public class Card : MonoBehaviourPunCallbacks, IPunObservable
{
    public int value;
    public int original_value=0;
    public GwLib.Variation variation;
    public GwLib.Mode mode;

    public int id;
    
    // gameobject Ui values
    public GameObject ed_value;
    public GameObject ed_original_value;
    public GameObject ed_variation;
    public GameObject ed_mode;

    // card size
    public float card_width;
    public float card_height;

    // owner player of this card
    public GameObject owner;

    // is waiting the count down
    public bool is_wait_count_down = false;

    // has replied remain virus attack value
    public bool has_replied_attack = false;
    
    // stop stream to avoid race value
    public bool stop_stream = false;

    [SerializeField]
    private List<GameObject> _to_del = new List<GameObject>();

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (!this.stop_stream)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(this.value);
                stream.SendNext(this.original_value);
                stream.SendNext(this.variation);
                stream.SendNext(this.mode);
            }
            else
            {
                this.value = (int)stream.ReceiveNext();
                this.original_value = (int)stream.ReceiveNext();
                this.variation = (GwLib.Variation)stream.ReceiveNext();
                this.mode = (GwLib.Mode)stream.ReceiveNext();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateCardUiValues();
        card_width = GetComponent<BoxCollider>().bounds.size.x;
        card_height = GetComponent<BoxCollider>().bounds.size.z;
    }

    // Update is called once per frame
    void Update()
    {
        this.UpdateCardUiValues();
    }

    void OnMouseDown()
    {
        // ACTIONS on card
        // showcase action
        if (this.owner.GetComponent<Player>().showcase_cards.Count >= 1 || this.has_replied_attack) {
            this.owner.GetComponent<Player>().PrepareDragAction();
        }    
    }

    // Update card text when call
    public void UpdateCardUiValues() 
    {
        ed_value.GetComponent<TMPro.TextMeshPro>().text = value.ToString(); 
        ed_original_value.GetComponent<TMPro.TextMeshPro>().text = "(" + original_value.ToString() + ")"; 
        ed_variation.GetComponent<TMPro.TextMeshPro>().text = variation.ToString(); 
        ed_mode.GetComponent<TMPro.TextMeshPro>().text = mode.ToString(); 
    }

    // move card from it's current position to a target position
    public void MoveCardTo(Vector3 target, float speed) 
    {
        StartCoroutine(Movement(target, speed));
    }

    // rotate card from it's current position to a target position
    public void RotateCardOf(float x, float y, float z, float speed) 
    {
        StartCoroutine(Rotation(x, y ,z, speed));
    }
    // call coroutine for attack
    public void RunSuccesAttack(int to, GameObject armor_card, GameObject life_card)
    {
        StartCoroutine(SuccesAttackWithCharge(to, new List<GameObject>(), armor_card, life_card));
    }
    // call coroutine for attack with charge
    public void RunSuccesAttackWithCharge(int to, List<GameObject> attack_cards, GameObject armor_card, GameObject life_card)
    {
        StartCoroutine(SuccesAttackWithCharge(to, attack_cards, armor_card, life_card));
    }
    public void RunAttackVirus(int to, List<GameObject> attack_cards, GameObject armor_card, GameObject life_card)
    {
        StartCoroutine(AttackVirus(to, attack_cards, armor_card, life_card));
    }
    // call coroutine for fail attack
    public void RunFailAttackWithCharge(int to, List<GameObject> attack_cards, GameObject armor_card)
    {
        StartCoroutine(FailAttackWithCharge(to, attack_cards, armor_card));
    }
    // call coroutine for destroy card
    public void DestroyCardIn(float time)
    {
        StartCoroutine(DestroyIn(time, this.gameObject));
    }

    [PunRPC]
    void StopStream()
    {
        this.stop_stream = true;
    }

    [PunRPC]
    void StartStream()
    {
        this.stop_stream = false;
    }

    [PunRPC]
    void RenameCard(string name)
    {
        this.gameObject.name = name;
    }

    [PunRPC]
    public void SetOwner(string player_name)
    {
        this.owner = GameObject.Find(player_name);
    }

    [PunRPC]
    public void Destroy()
    {
        GameObject.Destroy(this.gameObject);
    }

    // hide btn mode
    [PunRPC]
    public void DisableModeBtn(bool life=true, bool arm=true) 
    {
        if (life) Utils.HideObjectAndChildren(this.transform.Find("btn_life").gameObject, life);
        if (arm) Utils.HideObjectAndChildren(this.transform.Find("btn_arm").gameObject, arm);
    }

    // hide values on the card
    [PunRPC]
    public void DisableValues(bool ask) 
    {
        if (ask) this.transform.Find("as_base_card").Find("values_grp").gameObject.SetActive(false);
        else this.transform.Find("as_base_card").Find("values_grp").gameObject.SetActive(true);
    }

    // add card as life
    public void AddCardAsLife(GameObject player, GameObject target_player, GwLib.PlayerState player_state)
    {
        Debug.Log("Placing as Life.");
        // get card script
        Card card_script = this;
        
        // check if card is appendable
        if (!GwLib.IsAppendLife(card_script, target_player.GetComponent<Player>(), player_state))
        {
            Debug.LogWarning("Card cannot be added as life!");
            return;
        }

        // get new position
        Vector3 new_pos = target_player.GetComponent<Player>().GetNextLifePosition();
        // move to final pos
        card_script.MoveCardTo(new_pos, 10.0f);
        // set card mode
        card_script.mode = GwLib.Mode.Life;
        card_script.UpdateCardUiValues();
        // RPC add life card to clone players
        this.AddCardToLifeForClone(target_player.name);
        this.GetComponent<PhotonView>().RPC("AddCardToLifeForClone", RpcTarget.Others, target_player.name);
        // remove from showcase list
        player.GetComponent<Player>().showcase_cards.Remove(this.gameObject);
        // disable choose mode
        card_script.DisableModeBtn();
        card_script.DisableValues(false);
        // false the health power
        if (player.GetComponent<Player>().has_health_power) player.GetComponent<Player>().has_health_power = false;
        // unhide value for all players
        this.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
    }

    [PunRPC]
    public void AddCardToLifeForClone(string player_name)
    {
        GameObject player = GameObject.Find(player_name);
        player.GetComponent<Player>().life_cards.Add(this.gameObject);
    }

    [PunRPC]
    public void AddCardToArmorForClone(string player_name)
    {
        GameObject player = GameObject.Find(player_name);
        player.GetComponent<Player>().armor_cards.Add(this.gameObject);
    } 

    // clean all unused cards
    void CleanCardsAfterAttack(GameObject life_card, bool clean_opponenent_charge=false, bool clean_showcase=true)
    {
        Debug.Log("CLean Coroutine here!");
        Player opponent = life_card.GetComponent<Card>().owner.GetComponent<Player>();
        Player current_player = this.GetComponent<Card>().owner.GetComponent<Player>();
        // clean _to_del cards
        Debug.Log("CLEAN TO DEL COUNT : " + this._to_del.Count);
        for (int i=0; i < this._to_del.Count; i++) {
            GameObject del = this._to_del[i];
            Debug.Log("Clean to_del " + del);
            del.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
        }
        // clean charge
        if (clean_opponenent_charge) {
            for (int i=0; i < opponent.attack_cards.Count; i++) {
                GameObject charge = opponent.attack_cards[i];
                Debug.Log("Clean opponenent charge " + charge);
                charge.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
            }
            opponent.gameObject.GetComponent<PhotonView>().RPC("ClearAttackCards", RpcTarget.All);
        }
        for (int i=0; i < current_player.attack_cards.Count; i++) {
            GameObject charge = current_player.attack_cards[i];
            Debug.Log("Clean charge " + charge);
            charge.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
        }
        current_player.gameObject.GetComponent<PhotonView>().RPC("ClearAttackCards", RpcTarget.All);
        // clean anal
        for (int i=0; i < current_player.anal_cards.Count; i++) {
            GameObject anal = current_player.anal_cards[i];
            Debug.Log("current anal value " + anal.GetComponent<Card>().value);    
            if (anal.GetComponent<Card>().value <= 0) {
                Debug.Log("Clean current anal " + anal);
                current_player.gameObject.GetComponent<PhotonView>().RPC("RemoveFromAnalCards", RpcTarget.All, anal.name);
                anal.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
            }
        }
        for (int i=0; i < opponent.anal_cards.Count; i++) {
            GameObject anal = opponent.anal_cards[i];
            Debug.Log("opponent anal value " + anal.GetComponent<Card>().value);
            if (anal.GetComponent<Card>().value <= 0) {
                Debug.Log("Clean opponent anal " + anal);
                opponent.gameObject.GetComponent<PhotonView>().RPC("RemoveFromAnalCards", RpcTarget.All, anal.name);
                anal.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
            }
        }
        // clean show case and active card
        if (clean_showcase) {
            for (int i=0; i < current_player.showcase_cards.Count; i++) {
                GameObject show = current_player.showcase_cards[i];
                Debug.Log("Clean showcase " + show);
                current_player.gameObject.GetComponent<PhotonView>().RPC("RemoveFromShowcaseCards", RpcTarget.All, show.name);
                show.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
            }
        }
    }

    // movement coroutines
    IEnumerator Rotation(float x, float y, float z, float speed) 
    {
        Quaternion rotate_destination = Quaternion.Euler(x, y, z);
        while(rotate_destination != this.transform.rotation)
        {
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotate_destination, Time.deltaTime * speed);
            yield return 0;
        }
        // yield return null;    
    }
    public IEnumerator AnimDecreaseValue(int to, float speed=0.3f, float waiting=0.7f, bool stop_to_zero=true)
    {
        this.gameObject.GetComponent<PhotonView>().RPC("StopStream", RpcTarget.All);
        yield return new WaitForSeconds(waiting);
        while (this.value > to)
        {
            if (this == null) yield return 0;
            this.value -= 1;
            this.UpdateCardUiValues();
            if (stop_to_zero && this.value == 0) break;
            yield return new WaitForSeconds(speed);
        }
        yield return new WaitForSeconds(0f);
        this.gameObject.GetComponent<PhotonView>().RPC("StartStream", RpcTarget.All);
    }

    public IEnumerator AnimIncreaseValue(int to, float speed=0.2f, float waiting=0.6f)
    {
        this.gameObject.GetComponent<PhotonView>().RPC("StopStream", RpcTarget.All);
        yield return new WaitForSeconds(waiting);
        while (this.value < to)
        {
            this.value += 1;
            this.UpdateCardUiValues();
            yield return new WaitForSeconds(speed);
        }
        yield return new WaitForSeconds(0f);
        this.gameObject.GetComponent<PhotonView>().RPC("StartStream", RpcTarget.All);
    }

    IEnumerator Movement (Vector3 target, float speed, float threshold=0.01f)
    {
        while(Vector3.Distance(transform.position, target) > threshold)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, target, speed * Time.deltaTime);
            yield return 0;
        }
        // yield return null;    
    }

    IEnumerator DestroyIn (params object[] args)
    {
        yield return new WaitForSeconds((float)args[0]);
        for (int i = 1; i < args.Length; i++)
        {        
            GameObject.Destroy((GameObject)args[i]);
        }
        // yield return null;
    }

    // coroutine chain for sucess attack
    IEnumerator SuccesAttack(int true_damages, GameObject armor_card, GameObject life_card)
    {
        this._to_del = new List<GameObject>();
        List<GameObject> opponent_life_cards = life_card.GetComponent<Card>().owner.GetComponent<Player>().life_cards;
        yield return StartCoroutine(Movement(armor_card.transform.position + new Vector3(-0.1f, 0.1f, 0f), 15.0f));
        this.owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, this.gameObject.name, true_damages, true);
        yield return StartCoroutine(AnimDecreaseValue(true_damages));
        yield return StartCoroutine(Movement(life_card.transform.position + new Vector3(0f, 0.1f, 0f), 20.0f));
        // hide under
        this.transform.position = new Vector3(this.transform.position.x, -0.5f, this.transform.position.z);
        // save original value
        life_card.GetComponent<Card>().original_value = life_card.GetComponent<Card>().value;
        // hit life card
        int transfert_damages = true_damages - life_card.GetComponent<Card>().value;
        int life_remain = life_card.GetComponent<Card>().value - true_damages;
        life_card.GetComponent<Card>().owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, life_card.name, life_remain, true);
        yield return StartCoroutine(life_card.GetComponent<Card>().AnimDecreaseValue(life_remain));
        // stock card to delete
        if (life_card.GetComponent<Card>().value <= 0)
        {
            opponent_life_cards.Remove(life_card);
            life_card.GetComponent<Card>().owner.gameObject.GetComponent<PhotonView>().RPC("RemoveFromLifeCards", RpcTarget.All, life_card.name);
            this._to_del.Add(life_card);
        }
        if (transfert_damages > 0) {
            while (transfert_damages > 0) {
                if (opponent_life_cards.Count > 0) {
                    life_card = opponent_life_cards[0];
                    int life_value = life_card.GetComponent<Card>().value;
                    life_remain = life_value - transfert_damages;
                    transfert_damages = transfert_damages - life_value;
                    if (life_remain <= 0) {
                    life_card.GetComponent<Card>().owner.gameObject.GetComponent<PhotonView>().RPC("RemoveFromLifeCards", RpcTarget.All, life_card.name);
                        this._to_del.Add(life_card);
                    }
                    // save original value
                    life_card.GetComponent<Card>().original_value = life_card.GetComponent<Card>().value;
                    life_card.GetComponent<Card>().owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, life_card.name, life_remain, true);
                    yield return StartCoroutine(life_card.GetComponent<Card>().AnimDecreaseValue(life_remain));
                } else {
                    Debug.Log("DEAD UNDERGROUND -" + transfert_damages);
                    life_card.GetComponent<Card>().owner.gameObject.GetComponent<PhotonView>().RPC("RemoveFromLifeCards", RpcTarget.All, life_card.name);
                    this._to_del.Add(life_card);
                    // set underground life to opponenet player
                    life_card.GetComponent<Card>().owner.GetComponent<Player>().Die(-transfert_damages);
                    break;
                }
            }
        }
        if (opponent_life_cards.Count == 0 && transfert_damages == 0) {
            Debug.Log("DEAD ON THE GROUND");
            this._to_del.Add(life_card);
            life_card.GetComponent<Card>().owner.GetComponent<Player>().Die(0);
            yield return new WaitForSeconds(0f);
        }
    }
    IEnumerator PrepareAttackWithCharge(int true_damages, List<GameObject> attack_cards, GameObject armor_card)
    {
        this.original_value = this.value;
        foreach(GameObject charge in attack_cards) {
            charge.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
            yield return StartCoroutine(charge.GetComponent<Card>().Movement(armor_card.transform.position + new Vector3(-0.3f, 0.11f, 0f), 15.0f));
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(charge.GetComponent<Card>().Movement(armor_card.transform.position + new Vector3(0f, -1.0f, 0f), 15.0f));
        }
        Debug.Log("Increase full charge "+ true_damages);
        this.owner.GetComponent<PhotonView>().RPC("StartAnimIncrease", RpcTarget.Others, this.gameObject.name, true_damages);
        yield return StartCoroutine(this.AnimIncreaseValue(true_damages));
    }

    // coroutine chain for sucess attack with charge
    IEnumerator SuccesAttackWithCharge(int damages, List<GameObject> attack_cards, GameObject armor_card, GameObject life_card)
    {
        yield return StartCoroutine(Movement(armor_card.transform.position + new Vector3(-0.1f, 0.1f, 0f), 15.0f));
        yield return StartCoroutine(PrepareAttackWithCharge(damages, attack_cards, armor_card));
        int true_damages = damages - armor_card.GetComponent<Card>().value; // no clamp Utils.ClampInt(armor_card.GetComponent<Card>().value, 0, 13);
        // attack classic
        yield return StartCoroutine(SuccesAttack(true_damages, armor_card, life_card));
        // clean cards
        CleanCardsAfterAttack(life_card, clean_opponenent_charge:true);
        this.owner.GetComponent<Player>().game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.owner.gameObject.name + " attacked with " + damages.ToString() + " damages.", Logger.Level.Normal);
    }

    // coroutine chain for fail attack
    IEnumerator FailAttackWithCharge(int true_damages, List<GameObject> attack_cards, GameObject armor_card)
    {
        yield return StartCoroutine(Movement(armor_card.transform.position + new Vector3(-0.1f, 0.1f, 0f), 15.0f));
        yield return StartCoroutine(PrepareAttackWithCharge(true_damages, attack_cards, armor_card));
        this.owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, this.gameObject.name, true_damages, true);
        yield return StartCoroutine(AnimDecreaseValue(true_damages));
        // clean cards
        CleanCardsAfterAttack(armor_card);
        yield return StartCoroutine(DestroyIn(0.1f, this.gameObject));
        this.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.Others);
        this.owner.GetComponent<Player>().game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.owner.gameObject.name + " failed attack with " + true_damages.ToString() + " damages.", Logger.Level.Normal);
    }

    // coroutine chain for sucess attack virus
    IEnumerator AttackVirus(int damages, List<GameObject> attack_cards, GameObject armor_card, GameObject anal_card)
    {
        yield return StartCoroutine(Movement(anal_card.transform.position + new Vector3(-0.1f, 0.1f, 0f), 15.0f));
        yield return StartCoroutine(PrepareAttackWithCharge(damages, attack_cards, armor_card));
        // copy anal cards list to manage recursive damage
        List<GameObject> anal_cards = new List<GameObject>();
        foreach (GameObject item in anal_card.GetComponent<Card>().owner.GetComponent<Player>().anal_cards)
        {
            anal_cards.Add(item);
        }
        int anal_life_remain = anal_card.GetComponent<Card>().value - damages;
        int attack_remain = 0;
        if (anal_life_remain < 0)
            attack_remain = Math.Abs(anal_life_remain);
        this.owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, this.gameObject.name, attack_remain, true);
        StartCoroutine(this.AnimDecreaseValue(attack_remain));
        this.owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, anal_card.name, anal_life_remain, true);
        yield return StartCoroutine(anal_card.GetComponent<Card>().AnimDecreaseValue(anal_life_remain));
        int all_anal_damage = 0;
        foreach (GameObject item in anal_cards)
        {
            all_anal_damage += item.GetComponent<Card>().value;
        }
        int new_armor = armor_card.GetComponent<Card>().original_value - Utils.ClampInt(all_anal_damage, 0, 13);
        this.owner.GetComponent<PhotonView>().RPC("StartAnimIncrease", RpcTarget.Others, armor_card.name, new_armor);
        yield return StartCoroutine(armor_card.GetComponent<Card>().AnimIncreaseValue(new_armor));
        Debug.Log("Anal attack remains " + attack_remain);
        if (attack_remain > 0)
        {
            anal_cards.Remove(anal_card);
            while (anal_cards.Count > 0) {
                anal_card = anal_cards[0];
                anal_life_remain = anal_card.GetComponent<Card>().value - attack_remain;
                attack_remain = 0;
                if (anal_life_remain < 0)
                    attack_remain = Math.Abs(anal_life_remain);
                this.owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, this.gameObject.name, attack_remain, true);
                StartCoroutine(this.AnimDecreaseValue(attack_remain));
                this.owner.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, anal_card.name, anal_life_remain, true);
                yield return StartCoroutine(anal_card.GetComponent<Card>().AnimDecreaseValue(anal_life_remain));
                all_anal_damage = 0;
                foreach (GameObject item in anal_cards)
                {
                    all_anal_damage += item.GetComponent<Card>().value;
                }
                new_armor = armor_card.GetComponent<Card>().original_value - Utils.ClampInt(all_anal_damage, 0, 13);
                this.owner.GetComponent<PhotonView>().RPC("StartAnimIncrease", RpcTarget.Others, armor_card.name, new_armor);
                yield return StartCoroutine(armor_card.GetComponent<Card>().AnimIncreaseValue(armor_card.GetComponent<Card>().original_value - Utils.ClampInt(all_anal_damage, 0, 13)));
                Debug.Log("Anal attack remains " + attack_remain);
                if (anal_card.GetComponent<Card>().value <= 0) {
                    anal_cards.Remove(anal_card);
                }
                if (attack_remain <= 0) {
                    this.CleanCardsAfterAttack(anal_card, clean_showcase:true);
                    break;
                }
            }
            Debug.Log("Anal attack remains " + attack_remain);
            this.has_replied_attack = true;
            anal_cards.Clear();
            this.CleanCardsAfterAttack(armor_card, clean_showcase:false);
        } else {
            anal_cards.Clear();
            this.CleanCardsAfterAttack(armor_card, clean_showcase:true);
        }
        this.owner.GetComponent<Player>().game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.owner.gameObject.name + " attacked " + anal_card.GetComponent<Card>().owner.name + "'s anal virus.", Logger.Level.Normal);
        // attack classic
        // yield return StartCoroutine(SuccesAttack(damages, armor_card, life_card));
        // clean cards
        // CleanCardsAfterAttack(life_card, clean_opponenent_charge:true);
    }
}
