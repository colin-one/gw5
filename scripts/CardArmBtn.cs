﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;

public class CardArmBtn : MonoBehaviour
{
    private GameObject game_controller;

    // Start is called before the first frame update
    void Start()
    {
        // get game controller
        game_controller = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown() 
    {
        if (this.GetComponent<PhotonView>().IsMine)
        {
            Debug.Log("Placing as Armor.");
            // get current Player
            GameObject current_player = game_controller.GetComponent<GwMain>().current_player;
            // get card script
            Card card_script = this.GetComponentInParent<Card>();
            // get new position
            Vector3 new_pos = current_player.GetComponent<Player>().GetNextArmorPosition();

            // check if card is appendable
            if (!GwLib.IsAppendArmor(card_script, current_player.GetComponent<Player>(), game_controller.GetComponent<GwMain>().player_state))
            {
                Debug.LogWarning("Card cannot be added as armor!");
                return;
            }

            // move to final pos
            card_script.MoveCardTo(new_pos, 10.0f);
            card_script.RotateCardOf(0.0f, 90.0f, 0.0f, 10.0f);
            // set card mode
            card_script.mode = GwLib.Mode.Armor;
            card_script.UpdateCardUiValues();
            // put card in showcase active player
            card_script.gameObject.GetComponentInParent<PhotonView>().RPC("AddCardToArmorForClone", RpcTarget.All, current_player.name);
            // remove from showcase list
            current_player.GetComponent<Player>().showcase_cards.Remove(this.transform.parent.gameObject);
            // disable choose mode
            card_script.DisableModeBtn();
            // unhide value for all payers
            card_script.gameObject.GetComponentInParent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
        }
    } 
}
