﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardAttaBtn : MonoBehaviour
{
    private GameObject game_controller;

    // Start is called before the first frame update
    void Start()
    {
        // get game controller
        game_controller = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown() 
    {
        Debug.Log("Placing as Attack.");
        // get current Player
        GameObject current_player = game_controller.GetComponent<GwMain>().current_player;
        // set action 
        current_player.GetComponent<Player>().action_to_wait = GwLib.Action.AppendAttack;
        current_player.GetComponent<Player>().wait_an_action = true;
        // get card script
        Card card_script = this.GetComponentInParent<Card>();
        current_player.GetComponent<Player>().active_card = card_script.gameObject;
    } 
}
