﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;

public class CardLifeBtn : MonoBehaviour
{

    private GameObject game_controller;

    // Start is called before the first frame update
    void Start()
    {
        // get game controller
        game_controller = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown() 
    {
        if (this.GetComponent<PhotonView>().IsMine)
            this.SetCardAsLife();
    }

    public void SetCardAsLife() 
    {
        // get game controller
        game_controller = GameObject.FindGameObjectWithTag("GameController");
        GameObject player = this.game_controller.GetComponent<GwMain>().current_player;
        this.GetComponentInParent<Card>().AddCardAsLife(player, player, game_controller.GetComponent<GwMain>().player_state);
    }
}
