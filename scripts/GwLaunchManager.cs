﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

public class GwLaunchManager : MonoBehaviourPunCallbacks
{
    byte max_player_room = 6;
    bool is_connecting;
    public string player_name;
    string game_version = "1";

    // singleton
    private static GwLaunchManager instance;
    public static GwLaunchManager Instance {
        get {
            if(instance == null) {
                var gb = new GameObject("GwLaunchManager");
                instance = gb.AddComponent<GwLaunchManager>();
            }
            return instance;
        }
    }

    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void ConnectNetwork()
    {
        this.is_connecting = true;
        PhotonNetwork.NickName = player_name;
        if (PhotonNetwork.IsConnected)
        {
            Debug.Log("Joining room...");
            PhotonNetwork.JoinRandomRoom();
        } else {
            Debug.Log("Connectiong...");
            PhotonNetwork.GameVersion = this.game_version;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    /////
    public override void OnConnectedToMaster()
    {
        if (is_connecting)
        {
            Debug.Log("On connected to master...");
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to join an existing room...");
        PhotonNetwork.CreateRoom(null, new RoomOptions{ MaxPlayers=this.max_player_room, CleanupCacheOnLeave=false});
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarning("Disconned because " + cause);
        this.is_connecting = false;
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined room with " + PhotonNetwork.CurrentRoom.PlayerCount + " players");
        PhotonNetwork.LoadLevel("scn_gw5_proto");
    }
}
