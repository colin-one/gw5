﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using Photon.Pun;
using Photon.Realtime;

public class GwLib : MonoBehaviour
{
    public const int ELIMINATED_LIMIT = -14;
    public const int MAXCARDVALUE = 14;
    public const int MINCARDVALUE = 2;
    public enum Variation
    {
        Heart,
        Spade,
        Diamond,
        Clubs
    }

    public enum PlayerState
    {
        DrawThreeCards,
        DoAnAction,
        EndOfSeason,
        End,
    }

    public enum Mode
    {
        Life, 
        Armor, 
        Attack, 
        Anal,
        None
    }

    public enum Action
    {
        None,
        ReplaceArmor,
        ReplaceMyArmor,
        Attack,
        AppendAttack,
        AttackWithCharge,
        PutAnalVirus,
        AttackVirus,
        RepliedAttackVirus,
        AddLife,
    }

    // singleton
    private static GwLib instance;
    public static GwLib Instance {
        get {
           if(instance == null) {
              var gb = new GameObject("GwLib");
              instance = gb.AddComponent<GwLib>();
           }
           return instance;
        }
     }

    public static string winner_name;

    // check if new showcase card can be draw
    static public bool IsShowCaseDraw(Player player, PlayerState state) 
    {
        switch(state) 
        {
            case PlayerState.DrawThreeCards:
            {
                if (player.showcase_cards.Count + player.life_cards.Count + player.armor_cards.Count >= 3)
                {
                    return false;
                }
                if (player.showcase_cards.Count >= 3)
                {
                    return false;
                }
                return true;
            }

            case PlayerState.DoAnAction:
            {
                if (player.showcase_cards.Count >= 1)
                {
                    return false;
                }
                if (player.draw_cards >= 1)
                {
                    return false;
                }
                return true;
            }

            default: return true;
        }
    }

    // all is methods are done to check game rules
    // is the next card appendable to life ?
    static public bool IsAppendLife(Card card, Player player, PlayerState state) 
    {
        switch(state) 
        {
            case PlayerState.DrawThreeCards:
            {
                if (player.life_cards.Count >= 2) {
                    return false;
                }
                return true;
            }

            case PlayerState.DoAnAction:
            {
                return true;
            }

            case PlayerState.EndOfSeason:
            {
                return true;
            }

            default: return true;
        }
    }

    // is the next card appendable to life ?
    static public bool IsAppendArmor(Card card, Player player, PlayerState state) 
    {
        switch(state) 
        {
            case PlayerState.DrawThreeCards:
            {
                if (player.armor_cards.Count >= 1) {
                    return false;
                }
                return true;
            }

            case PlayerState.DoAnAction:
            {
                return true;
            }

            default: return true;
        }
    }

    // is the next card appendable to attack charge ?
    static public bool IsAppendAttack(Card card, Player player, PlayerState state) 
    {
        switch(state) 
        {
            case PlayerState.DrawThreeCards:
            {
                return false;
            }

            case PlayerState.DoAnAction:
            {
                if (player.draw_cards > 1)
                {
                    return false;
                }
                if (player.attack_cards.Count >= 4) { // maximal charge number
                    player.info = "Maximal charge reached !";
                    Debug.LogWarning("Maximal charge reached !");
                    return false;
                }
                return true;            
            }
            default: return true;
        }
    }

    // is the next card put as virus anal ?
    static public bool IsPuttableVirus(Card card, Player player, PlayerState state) 
    {
        switch(state) 
        {
            case PlayerState.DrawThreeCards:
            {
                return false;
            }
            case PlayerState.DoAnAction:
            {
                if (player.draw_cards > 1)
                {
                    return false;
                }
                return true;            
            }
            default: return true;
        }
    }

    // is the current card 
    static public bool IsReadyToDrop(Card card, Player player, PlayerState state) 
    {
        if (state == GwLib.PlayerState.DoAnAction) {
            if (player.showcase_cards.Contains(card.gameObject))
            {
                return true;
            }
            return false;
        }
        return false;
    }

    // is the current card 
    static public bool IsAllowToChangeArmor(GameObject card, PlayerState state) 
    {
        Card card_script = card.GetComponent<Card>();
        if (state == GwLib.PlayerState.DoAnAction) {
            if (card_script != null && card_script.mode == GwLib.Mode.Armor
                || (card.name == "as_armor_placeholder"))
            {
                return true;
            }
            return false;
        }
        return false;
    }

    // is the current card 
    static public bool IsAllowToAttack(Card card, PlayerState state, Player player) 
    {
        if (state == GwLib.PlayerState.DoAnAction) {
            if (card != null && card.mode == GwLib.Mode.Life)
            {
                if (player.attack_cards.Count == 3){
                    player.info = "Mega charge started you can't attack this turn.";
                    Debug.LogWarning("Mega charge started you can't attack this turn.");
                    return false;
                }
                return true;
            }
            return false;
        }
        return false;
    }

    // is the current card allow to attack virus
    static public bool IsAllowToAttackVirus(Card card, PlayerState state, Player player) 
    {
        if (state == GwLib.PlayerState.DoAnAction) {
            if (card != null && card.mode == GwLib.Mode.Anal)
            {
                if (player.attack_cards.Count == 3){
                    player.info = "Mega charge started you can't attack this turn.";
                    Debug.LogWarning("Mega charge started you can't attack this turn.");
                    return false;
                }
                return true;
            }
            return false;
        }
        return false;
    }

    // deduice action
    static public GwLib.Action GetWaitingAction(Card card, GameObject target, Player player, PlayerState state) 
    {
        Card target_script = target.GetComponent<Card>();
        if (state == GwLib.PlayerState.DoAnAction) {
            if (target_script != null && target_script.owner != player.gameObject && target_script.mode == GwLib.Mode.Armor
                || (target.name == "as_armor_placeholder"))
            {
                Debug.Log(player.gameObject.name + " change the opponent armor");
                return GwLib.Action.ReplaceArmor;
            }
            if (target_script != null && target_script.owner == player.gameObject && target_script.mode == GwLib.Mode.Armor
                || (target.name == "as_armor_placeholder" && player.GetComponent<Player>().armor_cards.Count == 0))
            {
                Debug.Log(player.gameObject.name + " change its own armor");
                return GwLib.Action.ReplaceMyArmor;
            }
            if (target_script != null && target_script.owner != player.gameObject && target_script.mode == GwLib.Mode.Life)
            {
                if (player.attack_cards.Count > 0) {
                    Debug.Log(player.gameObject.name + " attack with charge " + target_script.owner.name);
                    return GwLib.Action.AttackWithCharge;                
                }
                Debug.Log(player.gameObject.name + " attack " + target_script.owner.name);
                return GwLib.Action.Attack;
            }
            if (card != null && card.owner == player.gameObject)
            {
                if (target.name == "as_attack_placeholder" && card.mode != GwLib.Mode.Attack || (target_script != null && target_script.mode == GwLib.Mode.Attack))
                {
                    Debug.Log(player.gameObject.name + " load an attack");
                    return GwLib.Action.AppendAttack;
                }
            }
            if (card != null && card.owner == player.gameObject && !player.anal_played)
            {
                if (target.name == "as_virus_placeholder")
                {
                    if (target.transform.transform.parent.transform.parent.gameObject.GetComponent<Player>().armor_cards.Count > 0){
                        Debug.Log(player.gameObject.name + " put an anal virus");
                        return GwLib.Action.PutAnalVirus;
                    }
                }
            }
            if (card != null && card.owner == player.gameObject && player.has_health_power)
            {
                if (target.name == "as_life_placeholder")
                {
                    Debug.Log(player.gameObject.name + " put life");
                    return GwLib.Action.AddLife;
                }
            }
            if (target_script != null && target_script.mode == GwLib.Mode.Anal)
            {
                if (player.attack_cards.Count > 0) {
                    Debug.Log(player.gameObject.name + " attack virus anal with charge " + target_script.owner.name);
                    return GwLib.Action.AttackVirus;                
                }
                Debug.Log(player.gameObject.name + " attack virus anal " + target_script.owner.name);
                return GwLib.Action.AttackVirus;
            }
            if (target_script != null && target_script.owner != player.gameObject && target_script.mode == GwLib.Mode.Life)
            {
                if (player.attack_cards.Count > 0) {
                    Debug.Log(player.gameObject.name + " attack with charge " + target_script.owner.name);
                    return GwLib.Action.AttackWithCharge;                
                }
                Debug.Log(player.gameObject.name + " attack " + target_script.owner.name);
                return GwLib.Action.Attack;
            }
            return GwLib.Action.None;
        }
        return GwLib.Action.None;
    }

    // raycasting utilities
    // give position and object under the mouse
    public static (Vector3, GameObject) GetRayCastMouseInfo()
    {
        RaycastHit hit; 
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
        if ( Physics.Raycast (ray,out hit,1000.0f)) 
        {
            return (hit.point, hit.transform.gameObject);
        }
        return (new Vector3(0.0f, 0.0f, 0.0f), new GameObject());
    }

    // check dead and win people
    public static GameObject WhoIsTheWinner(List<GameObject> players)
    {   
        List<GameObject> winners = new List<GameObject>();
        foreach (GameObject player in players)
        {
            if (player.GetComponent<Player>().life_value > 0) {
                winners.Add(player);
            }
        }
        if (winners.Count == 1) return winners[0];
        else return null;
    }

    // resolve anal malus
    public static IEnumerator ResolveAnalDamage(List<GameObject> players, int start_id)
    {
        foreach (GameObject player in Utils.LoopListFrom(players, start_id))
        {
            if (!player.GetComponent<Player>().dead) {
                int armor_value = player.GetComponent<Player>().armor_value;
                if (armor_value < 0) {
                    int damage = Math.Abs(armor_value);
                    Debug.Log("Player " + player.name + " take " + damage + " damages");
                    yield return Instance.StartCoroutine(DispatchDamage(player, damage));
                }
            }
        }
    }

    // dispatch damage on the life
    public static IEnumerator DispatchDamage(GameObject player, int damage)
    {
        List<GameObject> _to_del = new List<GameObject>();
        List<GameObject> life_cards = new List<GameObject>();
        foreach (GameObject life in player.GetComponent<Player>().life_cards)
        {
            life_cards.Add(life);
        }
        if (damage > 0) {
            while (damage > 0) {
                if (life_cards.Count > 0) {
                    GameObject life_card = life_cards[0];
                    int life_value = life_card.GetComponent<Card>().value;
                    int life_remain = life_value - damage;
                    damage = damage - life_value;
                    if (life_remain <= 0) {
                        life_cards.Remove(life_card);
                        _to_del.Add(life_card);
                    }
                    // save original value
                    life_card.GetComponent<Card>().original_value = life_card.GetComponent<Card>().value;
                    player.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.Others, life_card.name, life_remain, true);
                    yield return Instance.StartCoroutine(life_card.GetComponent<Card>().AnimDecreaseValue(life_remain));
                } else {
                    Debug.Log("DEAD UNDERGROUND -" + damage);
                    // set underground life to opponenet player
                    player.GetComponent<Player>().Die(-damage);
                    break;
                }
            }
            if (life_cards.Count == 0 && damage == 0) {
                Debug.Log("DEAD ON THE GROUND");
                player.GetComponent<Player>().Die(0);
                yield return new WaitForSeconds(0f);
            }
        }
        // clean to del card
        for (int i = 0; i < _to_del.Count; i++)
        {
            GameObject life = _to_del[i];
            player.GetComponent<PhotonView>().RPC("RemoveFromLifeCards", RpcTarget.All, life.name);
            life.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
        }
    }

    // destroy an object list contains
    public static void DestroyList(List<GameObject> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            GameObject card = list[i];
            GameObject.Destroy(card);
        }
        list.Clear();
    }

    // collect remains cards
    public static List<GameObject> CollectRemainingCards(List<GameObject> players)
    {
        List<GameObject> remain_cards = new List<GameObject>();
        foreach (GameObject player in players)
        {
            foreach (GameObject card in player.GetComponent<Player>().anal_cards)
            {
                remain_cards.Add(card);
            }
            foreach (GameObject card in player.GetComponent<Player>().life_cards)
            {
                remain_cards.Add(card);
            }
            foreach (GameObject card in player.GetComponent<Player>().armor_cards)
            {
                remain_cards.Add(card);
            }
            foreach (GameObject card in player.GetComponent<Player>().attack_cards)
            {
                remain_cards.Add(card);
            }
        }
        return remain_cards;
    }

    // design and order list player
    public static void StartOrder(List<GameObject> players)
    {
        int min_armor = 100;
        List<GameObject> min_armor_go = new List<GameObject>();
        foreach (GameObject player in players)
        {
            int armor = player.GetComponent<Player>().armor_value;
            if (armor < min_armor) {
                min_armor_go.Clear();
                min_armor_go.Add(player);
                min_armor = armor;
            } else if (armor == min_armor) {
                min_armor_go.Add(player);
            }
        }
        // divide in case of egality
        if (min_armor_go.Count > 1) {
            min_armor_go.Sort((x, y) => x.GetComponent<Player>().life_value.CompareTo(y.GetComponent<Player>().life_value));
        }
        // first player
        GameObject first = min_armor_go[0];
        Debug.Log("First player is " + first.name);
        // decal cause next fonction start next iteration
        int id = players.IndexOf(first);
        int i = 0;
        foreach (GameObject player in Utils.LoopListFrom(players, id))
        {
            Debug.Log(player.name);
            player.GetComponent<Player>().index_order = i;
            i += 1;
        }
        players.Sort((x, y) => x.GetComponent<Player>().index_order.CompareTo(y.GetComponent<Player>().index_order));
    }
}
