﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using UnityEngine;


public class GwMain : MonoBehaviourPunCallbacks //, IPunObservable
{
    // logger
    public GameObject logger_go;
    Logger logger;
    public GameObject local_go;
    public List<string> player_names;
    public GameObject card_prefab;
    // all the players
    public List<GameObject> players;
    public List<GameObject> dead_players;
    public List<GameObject> eliminated_players;
    public List<GameObject> alive_players;
    // the action must be done by all players
    public GwLib.PlayerState player_state;
    public List<string> player_action_remain_names = new List<string>();
    // Pool object
    public GameObject pool;
    // target arrow object
    public GameObject target_arrow;
    // and the winner is ! 
    public GameObject winner;
    public bool is_started = false;
    // Start is called before the first frame update
    // stream name current player name
    public string current_player_name;
    // starts position
    public List<GameObject> ep_transforms;
    string str_tmp = "";

    // photon current player properties
    private GameObject _current_player;
    public GameObject current_player {
        get {
            return GameObject.Find(this.current_player_name);
        }
        set {
            _current_player = value;
        }
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        GameObject player = GameObject.Find(otherPlayer.NickName);
        player.GetComponent<Player>().Die(100);
        this.logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, PhotonNetwork.NickName + " left the room.", Logger.Level.Normal);
    }

    // dont destroy
    void Awake()
    {
        // DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        // logger script
        this.logger = this.logger_go.GetComponent<Logger>();
        // disable target
        target_arrow.SetActive(false);
        // collect all player
        // holder_players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        // holder_players.Sort((x, y) => x.name.CompareTo(y.name));
        StartMenu start_menu = GameObject.Find("sc_startmenu").GetComponent<StartMenu>();
        // foreach (GameObject item in holder_players)
        // {   
        //     item.transform.Find("as_indicator").gameObject.SetActive(false);
        //     item.SetActive(false);
        // }
        // for (int i = 0; i < StartMenu.Instance.players.Count; i++)
        // {
        //     string player_name = StartMenu.Instance.players[i].name;
        //     holder_players[i].name = player_name;
        //     holder_players[i].SetActive(true);
        //     this.players.Add(holder_players[i]);
        // }
        // clean object start menu
        string player_name = StartMenu.Instance.players[0].name;
        GameObject.Destroy(start_menu.gameObject);

        // online spawn player
        if (PhotonNetwork.IsConnected)
        {
            GameObject ep_player_position = ep_transforms[PhotonNetwork.CurrentRoom.PlayerCount - 1];
            GameObject player = PhotonNetwork.Instantiate("as_player", ep_player_position.transform.position, Quaternion.identity, 0);
            player.name = player_name;
            player.transform.localScale = ep_player_position.transform.localScale;
            // set nick name client network
            PhotonNetwork.NickName =  player_name;
            // keep track of go
            this.local_go = player;
        }
        this.logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, PhotonNetwork.NickName + " joined the room.", Logger.Level.Normal);
    }

    [PunRPC]
    void StartTheGame()
    {
            Debug.Log("Start game!");
            this.logger.Log(PhotonNetwork.MasterClient.NickName + " started the game.", Logger.Level.Normal);
            // close the room
            PhotonNetwork.CurrentRoom.IsOpen = false;

            this.is_started = true;
            players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
            // order to have same for all players
            players.Sort((x, y) => x.GetComponent<PhotonView>().ViewID.CompareTo(y.GetComponent<PhotonView>().ViewID));
            foreach (GameObject item in players)
            {   
                item.transform.Find("as_indicator").gameObject.SetActive(false);
            }
            // STEP 1 all players must draw three cards
            foreach (GameObject player in players) {
                // rename gameobject
                string photon_name = player.GetComponent<PhotonView>().Owner.NickName;
                player.name = photon_name;
                player_action_remain_names.Add(player.name);
            }
            player_state = GwLib.PlayerState.DrawThreeCards;
            // add alive players
            foreach (GameObject player in players) {
                alive_players.Add(player);
            }
            // position camera
            Vector3 center = Utils.ObjectsCenter(new List<GameObject>(UnityEngine.Object.FindObjectsOfType<GameObject>()));
            Camera camera = Camera.main;
            camera.gameObject.transform.position = new Vector3(center.x, camera.gameObject.transform.position.y, center.z);
    }

    // define current player
    [PunRPC]
    void SetCurrentPlayer()
    {
        this.current_player_name = player_action_remain_names[0];
        this.current_player = GameObject.Find(player_action_remain_names[0]);
        this.current_player.GetComponent<Player>().StartPlaying();
        this.logger.Log("It's " + this.current_player.gameObject.name + "'s turn!", Logger.Level.Warning);
    }

    // Update is called once per frame
    void Update()
    {
        // Check loosers and winners 
        if(this.player_state == GwLib.PlayerState.DoAnAction){
            GameObject winner = GwLib.WhoIsTheWinner(players);
            if (winner) {
                Debug.LogWarning("Winner ! is " + winner.name);
                GwLib.winner_name = winner.name;
                this.player_state = GwLib.PlayerState.End;
                SceneManager.LoadScene("scn_gw5_winner", LoadSceneMode.Single);
            }
        }

        // shortcut to end turn
        if (Input.GetKeyDown("space"))
        {
            this.current_player.GetComponent<Player>().StopPlaying();
        }
    }

    public void NextPlayer()
    {
        // prepare saeson if pool empty
        this.pool.GetComponent<PhotonView>().RPC("IsPoolEmpty", RpcTarget.All);
        this.gameObject.GetComponent<PhotonView>().RPC("PrepareNextPlayer", RpcTarget.All);
        // run resolve damage
        if (this.player_state == GwLib.PlayerState.DoAnAction || this.player_state == GwLib.PlayerState.EndOfSeason) 
        {
            this.pool.GetComponent<PhotonView>().RPC("LockPool", RpcTarget.All);
            Debug.Log("Go Next Player ! every malus will be resolved");
            StartCoroutine(ResolveAnalDamageAndUnlockPool());
        } 
    }

    IEnumerator ResolveAnalDamageAndUnlockPool()
    {
        yield return GwLib.ResolveAnalDamage(this.players, players.IndexOf(this.current_player));
        this.pool.GetComponent<PhotonView>().RPC("UnlockPool", RpcTarget.All);
    }

    [PunRPC]
    void PrepareNextPlayer()
    {
        // in case distribute new Season
        if (this.player_state == GwLib.PlayerState.EndOfSeason) 
        {
            this.pool.GetComponent<Pool>().season += 1;
            this.pool.GetComponent<Pool>().FillPool();
            StartCoroutine(this.pool.GetComponent<Pool>().DistributeSeasonCards(this.dead_players, this.alive_players));
            // reset power
            foreach (GameObject player in this.alive_players)
            {
                player.GetComponent<Player>().lucky_look_played = false;
                player.GetComponent<Player>().lucky_look_playing = false;
                player.GetComponent<Player>().anal_played = false;
                player.GetComponent<Player>().anal_playing = false;
            }
            this.player_state = GwLib.PlayerState.DoAnAction;
        }
        Debug.Log("Player finish to play.");
        this.RemoveFinishPlayer(current_player.name);
        // next player ?
        if (player_action_remain_names.Count != 0) 
        {
            Debug.Log("Next player must play.");
            this.SetCurrentPlayer();
        }
        if (player_action_remain_names.Count == 0)
        {
            // start ordering
            Debug.Log("start ordering " + this.alive_players);
            if (this.player_state == GwLib.PlayerState.DrawThreeCards) 
            {
                GwLib.StartOrder(this.alive_players);
            }
            Debug.Log("end ordering " + this.alive_players);
            this.player_state = GwLib.PlayerState.DoAnAction;
            this.StartDoAnAction();
        }
    }

    [PunRPC]
    void StartDoAnAction() 
    {
        // STEP 1 all players must draw one card
        foreach (GameObject player in this.alive_players) {
            player_action_remain_names.Add(player.name);
        }
        // this.current_player = GameObject.Find(player_action_remain_names[0]);
        // this.current_player_name = current_player.gameObject.name;
        // current_player.GetComponent<Player>().StartPlaying();
        this.SetCurrentPlayer();
        this.player_state = GwLib.PlayerState.DoAnAction;
    }

    [PunRPC]
    void RemoveFinishPlayer(string player_name)
    {
        this.player_action_remain_names.Remove(player_name);
    }

    [PunRPC]
    void RemoveEliminatedPlayers(string player_name)
    {
        this.eliminated_players.Remove(GameObject.Find(player_name));
    }

    [PunRPC]
    void AddEliminatedPlayers(string player_name)
    {
        this.eliminated_players.Add(GameObject.Find(player_name));
    }

    [PunRPC]
    void RemoveAlivePlayers(string player_name)
    {
        this.alive_players.Remove(GameObject.Find(player_name));
    }

    [PunRPC]
    void RemoveDeadPlayers(string player_name)
    {
        this.dead_players.Remove(GameObject.Find(player_name));
    }

    [PunRPC]
    void AddDeadPlayers(string player_name)
    {
        this.dead_players.Add(GameObject.Find(player_name));
    }

    void OnGUI() {
        if (!this.is_started && PhotonNetwork.IsMasterClient)
        {
            if (GUILayout.Button("Start the game")) {
                this.gameObject.GetComponent<PhotonView>().RPC("StartTheGame", RpcTarget.All);
                this.gameObject.GetComponent<PhotonView>().RPC("SetCurrentPlayer", RpcTarget.All);
                // this.SetCurrentPlayer();
            }
        }
        if(this.GetComponent<GwMain>().current_player_name != PhotonNetwork.NickName) return;
        if (GUILayout.Button("Finish Turn")) {
            if (current_player.GetComponent<Player>().lucky_look_played) {
                current_player.GetComponent<Player>().lucky_look_playing = false;
                current_player.GetComponent<Player>().lucky_look_played = true;
            }
            current_player.GetComponent<Player>().StopPlaying();
            this.GetComponent<GwMain>().NextPlayer();
        }
        if (current_player.GetComponent<Player>().draw_cards == 0 && this.player_state == GwLib.PlayerState.DoAnAction)
        {
            if (GUILayout.Button("Play Lucky Look")) {
                if (!current_player.GetComponent<Player>().lucky_look_played) {
                    current_player.GetComponent<Player>().lucky_look_playing = true;
                    current_player.GetComponent<Player>().lucky_look_played = true;
                } else {
                    current_player.GetComponent<Player>().info = "Already played the lucky look";
                }
            }
            if (current_player.GetComponent<Player>().lucky_look_playing) {
                str_tmp = GUILayout.TextField(str_tmp, 25);
                if (int.TryParse(str_tmp, out _)) {
                    current_player.GetComponent<Player>().lucky_look_value = Int32.Parse(str_tmp);
                }
            }
        }
    }
}
