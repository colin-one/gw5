﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;

public class Logger : MonoBehaviour
{
    public GameObject text_tpl;
    public enum Level
    {
        Warning,
        Normal,
        Error
    }

    [PunRPC]
    public void Log(string text, Level level)
    {
        System.TimeSpan tt = System.DateTime.Now.TimeOfDay;
        string log_text = tt.Hours.ToString() + ":" + tt.Minutes.ToString() + ":" + tt.Seconds.ToString() + " :: " + text;
        GameObject new_text = Instantiate(text_tpl);
        new_text.SetActive(true);
        new_text.transform.SetParent(text_tpl.transform.parent);
        new_text.GetComponent<TMPro.TextMeshProUGUI>().text = log_text;
        switch(level)
        {
            case Level.Warning:
            {
                new_text.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(0.5f, 0.5f, 0.1f);
                break;
            }
            case Level.Error:
            {
                new_text.GetComponent<TMPro.TextMeshProUGUI>().color = new Color(0.9f, 0.2f, 0.1f);
                break;
            }
        }
    }
}
