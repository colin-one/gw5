﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class Player : MonoBehaviour
{
    // attr players
    public int index_order;
    // life value
    public int life_value;
    public List<GameObject> life_cards;
    // armor value
    public int armor_value;
    public List<GameObject> armor_cards;
    // attack value
    public int attack_value;
    public List<GameObject> attack_cards;
    // showcase cards
    public List<GameObject> showcase_cards;
    // anal cards
    public List<GameObject> anal_cards;
    // is playing ? 
    public bool is_playing;
    // has draw first three card
    public bool has_draw_three;
    // has lucky look
    public bool lucky_look_played = false;
    public bool lucky_look_playing = false;
    public int lucky_look_value;
    // has anal virus
    public bool anal_played = false;
    public bool anal_playing = false;
    // number card drawn
    public int draw_cards = 0;
    // game controller
    public GameObject game_controller;
    // is wait a target 
    public bool wait_an_action = false;
    public GwLib.Action action_to_wait = GwLib.Action.None;
    // active card last cliked in showcase
    public GameObject active_card;
    // message to display
    public string head;
    public string info;
    // player die
    public bool dead = false;
    public int dead_under = 0;
    //health power
    public bool has_health_power = false;
    // disappointed look
    public bool has_disappointed = false;

    // Start is called before the first frame update
    void Start()
    {
        // get game controller
        game_controller = GameObject.FindGameObjectWithTag("GameController");
        // init info
        this.info = "";
    }

    // Update is called once per frame
    void Update()
    {
        // if current player
        // if (this.gameObject.GetComponent<PhotonView>().)
        // {

        // }
        // indicators
        // update info text
        this.head = string.Concat("Active player said : ", this.info);

        // update life stats
        int life = 0;
        foreach (GameObject card in this.life_cards)
        {
            Card card_script = card.GetComponent<Card>();
            life += card_script.value;
        }
        this.life_value = life;
        // update armor stats
        int armor = 0;
        foreach (GameObject card in this.armor_cards)
        {
            Card card_script = card.GetComponent<Card>();
            armor += card_script.value;
        }
        this.armor_value = armor;
        // update attack stats
        int attack = 0;
        foreach (GameObject card in this.attack_cards)
        {
            Card card_script = card.GetComponent<Card>();
            attack += card_script.value;
        }
        this.attack_value = attack;

        // check disappointed look
        if (this.life_value == 1 && game_controller.GetComponent<GwMain>().player_state == GwLib.PlayerState.DoAnAction)
        {
            if (!this.has_disappointed) {
                Debug.Log("Player " + this.gameObject.name + " has the disappointed look.");
                this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.gameObject.name + " has the regard du desespoire.", Logger.Level.Warning);
                this.info = "I've got the disappointed look.";
            }
            this.has_disappointed = true;
        } else { 
            this.info = "";
            this.has_disappointed = false;
        }

        // check health power
        if (this.life_value == this.armor_value && game_controller.GetComponent<GwMain>().player_state == GwLib.PlayerState.DoAnAction)
        {
            if (!this.has_health_power) {
                Debug.Log("Player " + this.gameObject.name + " has the health power.");
                this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.gameObject.name + " has the pouvoir de guerison.", Logger.Level.Warning);
                this.info = "I've got the health power.";
            }
            this.has_health_power = true;
        } else {
            if (this.has_health_power) {
                Debug.Log("Player " + this.gameObject.name + " lost his health power.");
                this.info = "";
            }
            this.has_health_power = false;
        }

        // check lucky look
        if (this.lucky_look_playing) {
            if (this.showcase_cards.Count > 0) {
                GameObject card = this.showcase_cards[0];
                card.GetComponent<Card>().DisableValues(false);
                // card.GetComponent<Card>().value = 10; // debug lucky look
                if (card.GetComponent<Card>().value == this.lucky_look_value) {
                    card.GetComponent<Card>().DisableModeBtn(life: false, arm: true);
                    card.GetComponent<Card>().GetComponentInChildren<CardLifeBtn>().SetCardAsLife();
                    card.GetComponent<Card>().DisableModeBtn(life: true, arm: true);
                }
            }
        }

        // wait a target for action and target arrow not null
        GameObject target = game_controller.GetComponent<GwMain>().target_arrow.GetComponent<TargetArrow>().target;
        if (this.wait_an_action && target != null) {
            GwLib.PlayerState player_state = game_controller.GetComponent<GwMain>().player_state;
            // deducce action to wait
            if (this.action_to_wait == GwLib.Action.None)
                this.action_to_wait = GwLib.GetWaitingAction(this.active_card.GetComponent<Card>(), target, this.GetComponent<Player>(), player_state);
            switch(this.action_to_wait)
            {
                case GwLib.Action.None:
                {
                    this.wait_an_action = false;
                    break;
                }
                case GwLib.Action.ReplaceArmor:
                {
                    if (GwLib.IsAllowToChangeArmor(target, game_controller.GetComponent<GwMain>().player_state)) {
                        this.ChangeOpponentArmor(this.active_card, target);
                        this.LooseAttackCharge();
                    }
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.ReplaceMyArmor:
                {
                    if (GwLib.IsAllowToChangeArmor(target, game_controller.GetComponent<GwMain>().player_state)) {
                        this.ChangeOpponentArmor(this.active_card, target);
                        this.LooseAttackCharge();
                    }
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.Attack:
                {
                    this.AttackWithCharge(this.active_card, target);
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.AttackWithCharge:
                {
                    if (GwLib.IsAllowToAttack(target.GetComponent<Card>(), game_controller.GetComponent<GwMain>().player_state, this)) {
                        if (this.attack_cards.Count == 4) {
                            this.AttackWithMegaCharge(this.active_card, target);
                        } else {
                            this.AttackWithCharge(this.active_card, target);
                        }
                    }
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.AppendAttack:
                {
                    if (GwLib.IsAppendAttack(this.active_card.GetComponent<Card>(), this, game_controller.GetComponent<GwMain>().player_state))
                    {
                        this.AppendAttack(this.active_card);
                    }
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.PutAnalVirus:
                {
                    if (GwLib.IsPuttableVirus(this.active_card.GetComponent<Card>(), this, game_controller.GetComponent<GwMain>().player_state))
                    {
                        this.anal_played = true;
                        this.PutAnalVirus(this.active_card,target);
                        this.LooseAttackCharge();
                    }
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.AttackVirus:
                {
                    if (GwLib.IsAllowToAttackVirus(target.GetComponent<Card>(), game_controller.GetComponent<GwMain>().player_state, this))
                    {
                        if (this.attack_cards.Count == 4) {
                            this.AttackVirusWithMegaCharge(this.active_card, target);
                        } else {
                            this.AttackVirus(this.active_card, target);
                        }                    
                    }
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                case GwLib.Action.AddLife:
                {
                    this.active_card.GetComponent<Card>().AddCardAsLife(this.gameObject, target.GetComponentInParent<Player>().gameObject,  game_controller.GetComponent<GwMain>().player_state);
                    if (this.game_controller.GetComponent<GwMain>().dead_players.Contains(target.GetComponentInParent<Player>().gameObject))
                    {
                        if (target.GetComponentInParent<Player>().life_cards[0].GetComponent<Card>().value > 0)
                        {
                            target.GetComponentInParent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.All, this.active_card.name, this.active_card.GetComponent<Card>().value + target.GetComponentInParent<Player>().dead_under, true);
                            target.GetComponentInParent<PhotonView>().RPC("Resurrect", RpcTarget.All);
                        } else {
                            target.GetComponentInParent<PhotonView>().RPC("SetDeadUnder", RpcTarget.All, target.GetComponentInParent<Player>().dead_under + this.active_card.GetComponent<Card>().value);
                            target.GetComponentInParent<PhotonView>().RPC("RemoveFromLifeCards", RpcTarget.All, this.active_card.name);
                            this.active_card.GetComponentInParent<PhotonView>().RPC("Destroy", RpcTarget.All);
                        }
                    }
                    if (target.GetComponentInParent<Player>() == this) this.LooseAttackCharge();
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
                default:
                {
                    this.wait_an_action = false;
                    this.action_to_wait = GwLib.Action.None;
                    break;
                }
            }
            game_controller.GetComponent<GwMain>().target_arrow.GetComponent<TargetArrow>().target = null;
            game_controller.GetComponent<GwMain>().target_arrow.SetActive(false);
        }
    }

    // generate the next position for the show cards
    public Vector3 GetNextShowcasePosition()
    {
        // get the ep object that is the init spawner
        GameObject ep_showcase = this.transform.Find("ep_player_showcase").gameObject;
        // more than 0 card inside showcase list, offset the next one of its width
        if (showcase_cards.Count > 0) {
            GameObject last_card = showcase_cards[showcase_cards.Count -1];
            Vector3 new_pos = new Vector3(last_card.transform.position.x, ep_showcase.transform.position.y, ep_showcase.transform.position.z)
                            + new Vector3(last_card.GetComponent<Card>().card_width, 0.0f, 0.0f);
            return new_pos;
        }
        else {
            return ep_showcase.transform.position;
        }
    }

    public Vector3 GetNextLifePosition()
    {
        // get the ep object that is the init spawner
        GameObject ep_life = this.transform.Find("ep_player_life").gameObject;
        // more than 0 card inside showcase list, offset the next one of its width
        if (life_cards.Count > 0) {
            GameObject first_card = life_cards[0];
            if (Vector3.Distance(first_card.transform.position, ep_life.transform.transform.position) >= 0.1f)
                return ep_life.transform.transform.position;
            // get all position and distance
            this.life_cards.Sort((x, y) => Vector3.Distance(x.transform.position, ep_life.transform.position).CompareTo(Vector3.Distance(y.transform.position, ep_life.transform.position)));         
            GameObject last_card = life_cards[life_cards.Count-1];
            Vector3 new_pos = last_card.transform.position + new Vector3(last_card.GetComponent<Card>().card_width, 0.0f, 0.0f);
            return new_pos;
        }
        else {
            return ep_life.transform.position;
        }
    }

    public Vector3 GetNextArmorPosition()
    {
        // get the ep object that is the init spawner
        GameObject ep_player_armor = this.transform.Find("ep_player_armor").gameObject;
        // more than 0 card inside showcase list, offset the next one of its width
        if (armor_cards.Count > 0) {
            GameObject last_card = armor_cards[armor_cards.Count -1];
            Vector3 new_pos = last_card.transform.position + new Vector3(last_card.GetComponent<Card>().card_height, 0.0f, 0.0f);
            return new_pos;
        }
        else {
            return ep_player_armor.transform.position;
        }
    }

    public Vector3 GetNextAttackPosition()
    {
        // get the ep object that is the init spawner
        GameObject ep_player_attack = this.transform.Find("ep_player_attack").gameObject;
        // more than 0 card inside showcase list, offset the next one of its width
        if (this.attack_cards.Count > 0) {
            GameObject last_card = this.attack_cards[this.attack_cards.Count -1];
            Vector3 new_pos = last_card.transform.position + new Vector3(0.1f, 0.05f, 0.0f);
            return new_pos;
        }
        else {
            return ep_player_attack.transform.position;
        }
    }

    public Vector3 GetNextVirusPosition()
    {
        // get the ep object that is the init spawner
        GameObject ep_player_virus = this.transform.Find("ep_player_virus").gameObject;
        if (this.anal_cards.Count > 0) {
            GameObject last_card = this.anal_cards[this.anal_cards.Count -1];
            Vector3 new_pos = last_card.transform.position + new Vector3(-0.3f, 0.11f, 0f);
            return new_pos;
        } else {
            return ep_player_virus.transform.position + new Vector3(0f, 0.1f, 0f);
        }
    }

    public void StopPlaying()
    {
        if (this.showcase_cards.Count > 0)
        {
            Debug.LogWarning("You must play the drawed card.");
            return;
        }
        Debug.Log("Stop playing!");
        this.is_playing = false;
        this.draw_cards = 0;
        // hide indicator
        this.gameObject.GetComponent<PhotonView>().RPC("DisableIndicator", RpcTarget.All);
    }

    public void StartPlaying()
    {
        this.is_playing = true;
        // unhide indicator
        this.gameObject.GetComponent<PhotonView>().RPC("EnableIndicator", RpcTarget.All);
    }

    [PunRPC]
    void SetDeadUnder(int value)
    {
        this.dead_under = value;
    }

    [PunRPC]
    void DisableIndicator()
    {
        this.gameObject.transform.Find("as_indicator").gameObject.SetActive(false);
    }

    [PunRPC]
    void EnableIndicator()
    {
        this.gameObject.transform.Find("as_indicator").gameObject.SetActive(true);
    }

    [PunRPC]
    void RemoveFromArmorCards(string card_name)
    {
        this.armor_cards.Remove(GameObject.Find(card_name));
    }

    [PunRPC]
    void AddToArmorCards(string card_name)
    {
        this.armor_cards.Add(GameObject.Find(card_name));
    }

    [PunRPC]
    void RemoveFromAnalCards(string card_name)
    {
        this.anal_cards.Remove(GameObject.Find(card_name));
    }

    [PunRPC]
    void AddToAnalCards(string card_name)
    {
        this.anal_cards.Add(GameObject.Find(card_name));
    }

    [PunRPC]
    void RemoveFromAttackCards(string card_name)
    {
        this.attack_cards.Remove(GameObject.Find(card_name));
    }

    [PunRPC]
    void ClearAttackCards()
    {
        this.attack_cards.Clear();
    }

    [PunRPC]
    void AddToAttackCards(string card_name)
    {
        this.attack_cards.Add(GameObject.Find(card_name));
    }

    [PunRPC]
    void RemoveFromShowcaseCards(string card_name)
    {
        this.showcase_cards.Remove(GameObject.Find(card_name));
    }

    [PunRPC]
    void ClearShowcaseCards()
    {
        this.showcase_cards.Clear();
    }

    [PunRPC]
    void RemoveFromLifeCards(string card_name)
    {
        this.life_cards.Remove(GameObject.Find(card_name));
    }

    [PunRPC]
    void ClearLifeCards()
    {
        this.life_cards.Clear();
    }

    [PunRPC]
    void AddToLifeCards(string card_name)
    {
        this.life_cards.Add(GameObject.Find(card_name));
    }

    [PunRPC]
    void StartAnimDecrease(string card_name, int value, bool stop_to_zero)
    {
        StartCoroutine(GameObject.Find(card_name).GetComponent<Card>().AnimDecreaseValue(value, stop_to_zero:stop_to_zero));
    }

    [PunRPC]
    void StartAnimIncrease(string card_name, int value)
    {
        StartCoroutine(GameObject.Find(card_name).GetComponent<Card>().AnimIncreaseValue(value));
    }

    public void PrepareDragAction()
    {
        if (Input.GetMouseButtonDown(0)) {
            // if selected card is the showcase card
            this.active_card = GwLib.GetRayCastMouseInfo().Item2;
            GwLib.PlayerState player_state = game_controller.GetComponent<GwMain>().player_state;
            if (GwLib.IsReadyToDrop(active_card.GetComponent<Card>(), this.GetComponent<Player>(), player_state)) {
                game_controller.GetComponent<GwMain>().target_arrow.SetActive(true);
                // wait an action
                this.wait_an_action = true;
            }
        }
    }

    public void LooseAttackCharge()
    {
        if (this.attack_cards.Count > 0) {
            foreach(GameObject charge in this.attack_cards)
            {
                charge.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
            }
            this.gameObject.GetComponent<PhotonView>().RPC("ClearAttackCards", RpcTarget.All);
        }
    }

    public void ChangeOpponentArmor(GameObject active_card, GameObject target)
    {
        Card active_card_script = active_card.GetComponent<Card>();
        active_card_script.MoveCardTo(target.transform.position, 10.0f);
        active_card_script.RotateCardOf(0.0f, 90.0f, 0.0f, 10.0f);

        Card target_script = target.GetComponent<Card>();
        // already an armor
        if (target_script != null) {
            Player target_player = target_script.owner.GetComponent<Player>();
            // remove card from armors
            target_player.armor_cards.Remove(target);
            target_player.gameObject.GetComponent<PhotonView>().RPC("RemoveFromArmorCards", RpcTarget.All, target.name);
            target_player.gameObject.GetComponent<PhotonView>().RPC("AddToArmorCards", RpcTarget.All, active_card.name);
            // unhide for other players
            active_card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.Others, false);
            // destroy old armor
            target.GetComponent<PhotonView>().RPC("Destroy", RpcTarget.All);
            active_card.GetComponent<PhotonView>().RPC("SetOwner", RpcTarget.All, target_player.gameObject.name);
            // check if virus anal
            if (target_player.anal_cards.Count > 0) {
                Debug.Log("Anal virus present, new armor value");
                int anal_value = 0;
                foreach (GameObject anal in  target_player.anal_cards)
                {
                    anal_value += anal.GetComponent<Card>().value;
                }
                active_card_script.original_value = active_card_script.value;
                this.gameObject.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.All, active_card_script.gameObject.name, 
                    active_card_script.value - anal_value, false);
                this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.gameObject.name + " changed " + target_player.gameObject.name + "'s armor.", Logger.Level.Normal);
            }
        } else {
            GameObject target_player = target.transform.parent.transform.parent.gameObject;
            active_card.GetComponent<PhotonView>().RPC("SetOwner", RpcTarget.All, target_player.name);
            target_player.GetComponent<Player>().armor_cards.Add(active_card);
            this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.gameObject.name + " add an amor to it.", Logger.Level.Normal);
        }
        active_card_script.DisableModeBtn(true, true);
        active_card_script.DisableValues(false);
        // remove active card from showcase
        this.GetComponent<Player>().showcase_cards.Remove(active_card);
        // set new owner card
        // set card mode as armor and update UI
        active_card_script.mode = GwLib.Mode.Armor;
        active_card_script.UpdateCardUiValues();
    }

    public void AppendAttack(GameObject active_card)
    {
        // get new position
        Vector3 new_pos = this.GetComponent<Player>().GetNextAttackPosition();
        // get card script
        Card card_script = active_card.GetComponentInParent<Card>();
        // move to final pos
        card_script.MoveCardTo(new_pos, 10.0f);
        card_script.RotateCardOf(0.0f, 20.0f, 0.0f, 10.0f);
        // set card mode
        card_script.mode = GwLib.Mode.Attack;
        card_script.UpdateCardUiValues();
        // put card in showcase active player
        card_script.DisableModeBtn(true, true);
        this.gameObject.GetComponent<PhotonView>().RPC("AddToAttackCards", RpcTarget.All, active_card.name);
        // remove from showcase list
        this.GetComponent<Player>().showcase_cards.Remove(active_card);
        this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.gameObject.name + " loaded an attack.", Logger.Level.Normal);
    }

    public void PutAnalVirus(GameObject active_card, GameObject target)
    {
        // get new position
        Player opponent_player = target.GetComponentInParent<Player>();
        Vector3 new_pos = opponent_player.GetComponent<Player>().GetNextVirusPosition();
        // get card script
        Card card_script = active_card.GetComponentInParent<Card>();
        // move to final pos
        card_script.MoveCardTo(new_pos, 10.0f);
        card_script.RotateCardOf(0.0f, 90.0f, 0.0f, 10.0f);
        // set card mode
        card_script.mode = GwLib.Mode.Anal;
        card_script.UpdateCardUiValues();
        // get opponent armor to ubstract virus value
        GameObject opponent_armor = opponent_player.armor_cards[0];
        int new_armor_value = 0;
        if (opponent_armor.GetComponent<Card>().original_value == 0) {
            opponent_armor.GetComponent<Card>().original_value = opponent_armor.GetComponent<Card>().value;
            new_armor_value = opponent_armor.GetComponent<Card>().original_value - card_script.value;
        } else {
            new_armor_value = opponent_armor.GetComponent<Card>().value - card_script.value;
        }
        this.gameObject.GetComponent<PhotonView>().RPC("StartAnimDecrease", RpcTarget.All, opponent_armor.name, new_armor_value, false);
        // put card in showcase active player
        card_script.DisableModeBtn(true, true);
        opponent_player.GetComponent<PhotonView>().RPC("AddToAnalCards", RpcTarget.All, active_card.name);
        // remove from showcase list
        this.GetComponent<Player>().showcase_cards.Remove(active_card);
        // change card owner
        active_card.GetComponent<PhotonView>().RPC("SetOwner", RpcTarget.All, opponent_player.gameObject.name);
        active_card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
        this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, this.gameObject.name + " put anal virus on " + opponent_player.gameObject.name + ".", Logger.Level.Normal);
    }

    public void AttackWithCharge(GameObject active_card, GameObject target)
    {
        // get armor card
        Player target_player = target.GetComponent<Card>().owner.GetComponent<Player>();
        // add damages
        int damages = active_card.GetComponent<Card>().value;
        foreach(GameObject charge in this.attack_cards) {
            damages += charge.GetComponent<Card>().value;
        }
        GameObject armor_card = target_player.armor_cards[0];
        int true_damages =  damages - armor_card.GetComponent<Card>().value; // no clamp Utils.ClampInt(armor_card.GetComponent<Card>().value, 0, 13);
        Debug.Log("Attack + charge - defense will do " + damages);
        // display values
        active_card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
        active_card.GetComponent<Card>().mode = GwLib.Mode.Attack;
        // active_card.GetComponent<Card>().UpdateCardUiValues();
        // check if mega charge
        // get dif and count down
        if (true_damages >  0) {
            // anim with next target damage
            Debug.Log("Attack with charge successful.");
            active_card.GetComponent<Card>().RunSuccesAttackWithCharge(damages, attack_cards, armor_card, target);
        } else {
            Debug.Log("Attack with charge fail.");
            active_card.GetComponent<Card>().RunFailAttackWithCharge(true_damages, attack_cards,  armor_card);
        }
        // remove from showcase list
        this.GetComponent<Player>().showcase_cards.Remove(active_card);
    }

    public void AttackWithMegaCharge(GameObject active_card, GameObject target)
    {
        // get armor card
        Player target_player = target.GetComponent<Card>().owner.GetComponent<Player>();
        // add damages
        GameObject last_charge = this.attack_cards[this.attack_cards.Count - 1];
        int damages = active_card.GetComponent<Card>().value * last_charge.GetComponent<Card>().value;
        GameObject armor_card = target_player.armor_cards[0];
        int true_damages =  damages - armor_card.GetComponent<Card>().value;
        Debug.Log("Attack + mega charge - defense will do " + damages);
        // display values
        active_card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
        active_card.GetComponent<Card>().mode = GwLib.Mode.Attack;
        this.info = "Attack with MEGA charge!";
        // get dif and count down
        if (true_damages >  0) {
            // anim with next target damage
            Debug.Log("Attack with Mega charge successful.");
            active_card.GetComponent<Card>().RunSuccesAttackWithCharge(damages, new List<GameObject>(){last_charge}, armor_card, target);
        } else {
            Debug.Log("Attack with charge fail.");
            active_card.GetComponent<Card>().RunFailAttackWithCharge(true_damages, new List<GameObject>(){last_charge},  armor_card);
        }
        // remove from showcase list
        this.GetComponent<Player>().showcase_cards.Remove(active_card);
    }

    public void AttackVirus(GameObject active_card, GameObject target)
    {
        // get armor card
        Player target_player = target.GetComponent<Card>().owner.GetComponent<Player>();
        // add damages
        int damages = active_card.GetComponent<Card>().value;
        foreach(GameObject charge in this.attack_cards) {
            damages += charge.GetComponent<Card>().value;
        }
        GameObject armor_card = target_player.armor_cards[0];
        Debug.Log("Attack virus + charge = " + damages);
        // display values
        active_card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
        active_card.GetComponent<Card>().mode = GwLib.Mode.Attack;
        // attack virus
        active_card.GetComponent<Card>().RunAttackVirus(damages, attack_cards, armor_card, target);
    }

    public void AttackVirusWithMegaCharge(GameObject active_card, GameObject target)
    {
        // get armor card
        Player target_player = target.GetComponent<Card>().owner.GetComponent<Player>();
        // add damages
        GameObject last_charge = this.attack_cards[this.attack_cards.Count - 1];
        int damages = active_card.GetComponent<Card>().value * last_charge.GetComponent<Card>().value;
        GameObject armor_card = target_player.armor_cards[0];
        Debug.Log("Attack virus + mega charge = " + damages);
        // display values
        active_card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.All, false);
        active_card.GetComponent<Card>().mode = GwLib.Mode.Attack;
        // attack virus
        active_card.GetComponent<Card>().RunAttackVirus(damages, new List<GameObject>(){last_charge}, armor_card, target);
    }

    public void Die(int damage) 
    {
        this.life_value = 0;
        // this.dead_under = damage;
        this.gameObject.GetComponent<PhotonView>().RPC("SetDeadUnder", RpcTarget.All, damage);
        this.dead = true;
        if (damage < GwLib.ELIMINATED_LIMIT) {
            // eliminated
            this.game_controller.gameObject.GetComponent<PhotonView>().RPC("AddEliminatedPlayers", RpcTarget.All, this.gameObject.name);
            this.game_controller.gameObject.GetComponent<PhotonView>().RPC("RemoveAlivePlayers", RpcTarget.All, this.gameObject.name);
            if (this.game_controller.GetComponent<GwMain>().player_action_remain_names.Contains(this.gameObject.name))
                this.game_controller.gameObject.GetComponent<PhotonView>().RPC("RemoveFinishPlayer", RpcTarget.All, this.gameObject.name);
        } else {
            this.game_controller.gameObject.GetComponent<PhotonView>().RPC("AddDeadPlayers", RpcTarget.All, this.gameObject.name);
            this.game_controller.gameObject.GetComponent<PhotonView>().RPC("RemoveAlivePlayers", RpcTarget.All, this.gameObject.name);
            if (this.game_controller.GetComponent<GwMain>().player_action_remain_names.Contains(this.gameObject.name))
                this.game_controller.gameObject.GetComponent<PhotonView>().RPC("RemoveFinishPlayer", RpcTarget.All, this.gameObject.name);
        }
        this.gameObject.GetComponent<PhotonView>().RPC("DestroyPlayerList", RpcTarget.All);
    }

    [PunRPC]
    public void DestroyPlayerList()
    {
        GwLib.DestroyList(this.anal_cards);
        GwLib.DestroyList(this.armor_cards);
        GwLib.DestroyList(this.life_cards);
        GwLib.DestroyList(this.attack_cards);
    }

    [PunRPC]
    public void Resurrect() 
    {
        this.dead_under = 0;
        this.dead = false;
        this.lucky_look_played = false;
        this.lucky_look_playing = false;
        this.anal_played = false;
        this.anal_playing = false;
        this.game_controller.GetComponent<GwMain>().dead_players.Remove(this.gameObject);
        this.game_controller.GetComponent<GwMain>().alive_players.Insert(this.index_order, this.gameObject);
        if (this.game_controller.GetComponent<GwMain>().player_action_remain_names.Count > 0) {
            this.game_controller.GetComponent<GwMain>().player_action_remain_names.Add(this.gameObject.name);
            this.game_controller.GetComponent<GwMain>().player_action_remain_names.Sort((x, y) => 
                GameObject.Find(x).GetComponent<Player>().index_order.CompareTo(GameObject.Find(y).GetComponent<Player>().index_order));
        }
    }
}
