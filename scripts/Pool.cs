﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

using Photon.Pun;
using Photon.Realtime;

public class Pool : MonoBehaviourPunCallbacks  //, IPunObservable
{
    public List<int> data_pool_value = new List<int>(); 
    public List<int> data_pool_variation = new List<int>(); 
    // game controller
    private GameObject game_controller;
    // pool state draw three or one card
    public GwLib.PlayerState pool_state;
    // lock pool during coroutine
    public bool locked = false;
    // seasons number
    public int season = 0;

    // public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    // {
    //     if (stream.IsWriting)
    //     {
    //         stream.SendNext(this.data_pool_value.ToArray());
    //         stream.SendNext(this.data_pool_variation.ToArray());
    //     }
    //     else
    //     {
    //         this.data_pool_value = new List<int>((int[])stream.ReceiveNext());
    //         this.data_pool_variation = new List<int>((int[])stream.ReceiveNext());
    //     }
    // }

    // Start is called before the first frame update
    void Start()
    {
        this.FillPool();
    }

    // Update is called once per frame
    void Update()
    {

    }

    // mouse click
    void OnMouseDown() 
    {   
        // check if it the right player else quit
        if(this.game_controller.GetComponent<GwMain>().current_player_name != PhotonNetwork.NickName) return;
        //
        if (this.locked) return;
        this.pool_state = this.game_controller.GetComponent<GwMain>().player_state;
        switch(this.pool_state)
        {
            // a card in drawthreecards mode
            case GwLib.PlayerState.DrawThreeCards:
            {
                // get current Player
                GameObject current_player = game_controller.GetComponent<GwMain>().current_player;
                if (!GwLib.IsShowCaseDraw(current_player.GetComponent<Player>(), pool_state))
                {
                        Debug.LogWarning("Player already draw three cards.");
                        return;
                }
                this.DrawACard(current_player, false);
                break;
            }
            // a card in DoAnAction mode
            case GwLib.PlayerState.DoAnAction:
            {
                // get current Player
                GameObject current_player = game_controller.GetComponent<GwMain>().current_player;
                if (!GwLib.IsShowCaseDraw(current_player.GetComponent<Player>(), pool_state))
                {
                        Debug.LogWarning("Player already draw a card.");
                        return;
                }
                GameObject card = this.DrawACard(current_player, true);
                card.GetComponent<Card>().DisableModeBtn(life: true, arm: true);
                // check health power
                if (current_player.GetComponent<Player>().has_health_power) {
                    // visible and life possibility
                    card.GetComponent<Card>().DisableModeBtn(life: true, arm:true);
                }
                // check disappointed look (order is important, before lucky look)
                if (current_player.GetComponent<Player>().has_disappointed) {
                    // visible
                    card.GetComponent<Card>().DisableValues(false);
                }
                // add one to draw cards
                current_player.GetComponent<Player>().draw_cards += 1;
                break;
            }
            default: break;
        }
    }

    [PunRPC]
    public void IsPoolEmpty() 
    {
        if (data_pool_value.Count <= 0 && this.game_controller.GetComponent<GwMain>().player_state != GwLib.PlayerState.EndOfSeason)
        {
            Debug.Log("Pool cards empty, end of Season");
            this.game_controller.GetComponent<GwMain>().player_state = GwLib.PlayerState.EndOfSeason;
        }
    }

    [PunRPC]
    public void LockPool()
    {
        this.locked = true;
    }

    [PunRPC]
    public void UnlockPool()
    {
        this.locked = false;
    }

    public void FillPool()
    {
        // init pool state as draw three cards
        pool_state = GwLib.PlayerState.DrawThreeCards;
        // get game controller
        game_controller = GameObject.FindGameObjectWithTag("GameController");
        // populate the pool data with the 52 cards
        Debug.Log("Populating pool cards.");
        int total = 0;
        for (var value = GwLib.MINCARDVALUE; value <= GwLib.MAXCARDVALUE; value++) { 
            foreach (int variation in Enum.GetValues(typeof(GwLib.Variation))) {
                Debug.Log(string.Format("Create Card : {0} {1}", value, variation));
                // set value and variation to card
                data_pool_value.Add(value);
                data_pool_variation.Add(variation);
                total += 1;
            }
        }
        Debug.Log(string.Format("Total card created : {0}", total));
    }

    GameObject DrawACard(GameObject current_player, bool disable_value)
    {
        Debug.Log("Instanciate prefab card");
        this.game_controller.GetComponent<GwMain>().logger_go.GetComponent<PhotonView>().RPC("Log", RpcTarget.All, current_player.name + " draw a card.", Logger.Level.Normal);
        int id = UnityEngine.Random.Range(0, data_pool_value.Count);
        int card_data_val = data_pool_value[id];
        int card_data_var = data_pool_variation[id];
        // remove data card from pool and create card object
        this.GetComponent<PhotonView>().RPC("DeleteDataFromPool", RpcTarget.All, id);
        // instanciate prefab card and position it to showcase
        Vector3 new_pos = current_player.GetComponent<Player>().GetNextShowcasePosition();
        GameObject card = PhotonNetwork.Instantiate("as_action_cards", transform.position, Quaternion.identity);
        // get card script
        Card card_script = card.GetComponent<Card>();
        // add id info
        card_script.id = id;
        card_script.DisableValues(disable_value);
        card_script.mode = GwLib.Mode.None;
        // move to final pos
        card_script.MoveCardTo(new_pos, 10.0f);
        // update the new card with the data pool values
        card_script.value = card_data_val;
        card_script.original_value = card_data_val;
        card_script.variation = (GwLib.Variation)card_data_var;
        card_script.UpdateCardUiValues();
        // set the card owner
        card.GetComponent<PhotonView>().RPC("SetOwner", RpcTarget.All, current_player.name);
        // put card in showcase active player
        current_player.GetComponent<Player>().showcase_cards.Add(card);
        // general id card name
        card.GetComponent<PhotonView>().RPC("RenameCard", RpcTarget.All, card_data_val.ToString() + card_data_var.ToString() + this.season.ToString());
        // hide value for all other player
        card.GetComponent<PhotonView>().RPC("DisableValues", RpcTarget.Others, true);
        card.GetComponent<PhotonView>().RPC("DisableModeBtn", RpcTarget.Others, true, true);
        return card;
    }

    [PunRPC]
    void DeleteDataFromPool(int id)
    {
        // remove data card from pool and create card object
        data_pool_value.RemoveAt(id);
        data_pool_variation.RemoveAt(id);
    }
    
    IEnumerator FromPoolToLife(GameObject card, GameObject player, int under_ground_damage)
    {
        yield return new WaitForSeconds(1.0f);
        int life_remain = card.GetComponent<Card>().value + under_ground_damage;
        yield return card.GetComponent<Card>().AnimDecreaseValue(life_remain, stop_to_zero:false);
        if (life_remain > 0) {
            card.GetComponent<Card>().AddCardAsLife(player, player, this.game_controller.GetComponent<GwMain>().player_state);
            if (player.GetComponent<Player>().dead) player.GetComponent<Player>().Resurrect();
        } else {
            player.GetComponent<Player>().dead_under = life_remain;
            GameObject.Destroy(card);
            player.GetComponent<Player>().showcase_cards.Clear();
        }
    }

    public IEnumerator DistributeSeasonCards(List<GameObject> dead_players, List<GameObject> alive_players)
    {
        foreach (GameObject player in dead_players)
        {
            if (player.GetComponent<PhotonView>().IsMine)
            {
                GameObject card = this.DrawACard(player, false);
                card.GetComponent<Card>().DisableModeBtn(life: true, arm: true);
                // this.gameObject.GetComponent<PhotonView>().RPC("DeleteDataFromPool", RpcTarget.Others, card.GetComponent<Card>().id);
                StartCoroutine(FromPoolToLife(card, player, player.GetComponent<Player>().dead_under));
            }
            yield return new WaitForSeconds(1.0f);
        }
        foreach (GameObject player in alive_players)
        {
            if (player.GetComponent<PhotonView>().IsMine)
            {
                GameObject card = this.DrawACard(player, false);
                // if (Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == card.name).Count() > 1)
                // {
                //     GameObject.Destroy(card);
                //     card = this.DrawACard(player, false);
                // }
                card.GetComponent<Card>().DisableModeBtn(life: true, arm: true);
                // this.gameObject.GetComponent<PhotonView>().RPC("DeleteDataFromPool", RpcTarget.Others, card.GetComponent<Card>().id);
                StartCoroutine(FromPoolToLife(card, player, 0));
            } 
            yield return new WaitForSeconds(1.0f);
        }
    }
}
