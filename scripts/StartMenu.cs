﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class StartMenu : MonoBehaviour
{
    public bool connecting = false;
    [System.Serializable]
    public class PlayerStruct
    {
        public string name;
        public PlayerStruct(string name)
        {
            this.name = name;
        }
    }

    // singleton
    private static StartMenu instance;
    public static StartMenu Instance {
        get {
           if(instance == null) {
              var gb = new GameObject("sc_startmenu");
              instance = gb.AddComponent<StartMenu>();
           }
           return instance;
        }
     }

    public int player_number = 1;
    public List<PlayerStruct> players;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        // add two player at least
        PlayerStruct p1 = new PlayerStruct("player1");
        string default_name = "player1";
        if (PlayerPrefs.HasKey("PlayerName"))
        {
            default_name = PlayerPrefs.GetString("PlayerName");
            p1.name = PlayerPrefs.GetString("PlayerName");
        }
        PhotonNetwork.NickName =  default_name;
        this.players.Add(p1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI () 
    {
        GUI.Box (new Rect (0.18f * Screen.width,0.3f * Screen.height,620,250), "Guild Warrior 5");
        if (connecting)
        {
            GUI.Button (new Rect (0.18f * Screen.width + 310,0.3f * Screen.height + 40, 100, 40), "Connection...");
        } else {
            if (GUI.Button (new Rect (0.18f * Screen.width + 310,0.3f * Screen.height + 40, 100, 40), "Join Room!")) 
            {
                Debug.Log("Let's play.");
                // SceneManager.LoadScene("scn_gw5_proto", LoadSceneMode.Single);
                PlayerPrefs.SetString("PlayerName", this.players[0].name);
                PhotonNetwork.NickName = this.players[0].name;
                this.connecting = true;
                GwLaunchManager.Instance.ConnectNetwork();
            }
        }
        for (int i = 0; i < this.players.Count; i++)
        {
            PlayerStruct player = this.players[i];
            GUI.Label (new Rect (0.18f * Screen.width + 200, 0.46f * Screen.height + (20 * i) ,100,20), "Player name: "); 
            player.name = GUI.TextField(new Rect (0.18f * Screen.width + 280, 0.46f * Screen.height + (20 * i) ,160,20), player.name);
            if (i>=2)
                if(GUI.Button(new Rect (0.18f * Screen.width + 440, 0.46f * Screen.height + (20 * i) ,80,20), "Remove")) {
                    this.player_number -= 1;
                    this.players.Remove(player);
                }
        }
        if (GUI.Button (new Rect (0.18f * Screen.width + 310,0.3f * Screen.height + 140, 120, 20), "Open Gw5 rules"))
        {
            Application.OpenURL("https://docs.google.com/document/d/1OBMA7Ve7kH0_zancHDCzyCCwswirhK-ftgDs1cb65V0/edit");
        }
    }
}

