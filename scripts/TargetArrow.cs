﻿using UnityEngine;
using System.Collections;

public class TargetArrow : MonoBehaviour
{
    // the object hit by the arrow
    public GameObject target;
    LineRenderer lineRenderer;
    private bool is_start_drag = false;
    private Vector3 start_position;
    // start time to get an offset time before the display of arrow
    private float start_time;

    void Start()
    {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.widthMultiplier = 0.2f;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)){
            this.start_position = GwLib.GetRayCastMouseInfo().Item1;
            this.is_start_drag = true;
            this.start_time = Time.time;
        }
        if (Input.GetMouseButtonUp(0))
        {
            this.is_start_drag = false;
            lineRenderer.enabled = false;
            target = GwLib.GetRayCastMouseInfo().Item2;
        } 
        if (this.is_start_drag)
        {
            if (Time.time - this.start_time > 0.3f)
            {
                lineRenderer.enabled = true;
                lineRenderer.SetPosition(0, this.start_position);
                lineRenderer.SetPosition(1, GwLib.GetRayCastMouseInfo().Item1);
            }
        }
    }
}


