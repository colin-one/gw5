﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Utils {
    public static int ClampInt(int val, int min, int max)
    {
        if (val.CompareTo(min) < 0) return min;
        else if(val.CompareTo(max) > 0) return max;
        else return val;
    }

    public static IEnumerable LoopListFrom<T>(List<T> a, int start)
    {
        for(int i=start; i<a.Count; i++) 
            yield return a[i];
        for(int i=0; i<start; i++) 
            yield return a[i];
    }

    public static Vector3 ObjectsCenter(List<GameObject> objects)
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        foreach (GameObject item in objects)
        {
            if (item.layer == 5) continue; // skip UI
            bounds.Encapsulate(item.transform.position); 
        }
        return bounds.center;   
    }

    public static void HideObjectAndChildren(GameObject go, bool hide)
    {
        MeshRenderer[] comps = go.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer comp in comps)
        {
            comp.enabled = !hide;
        }
    }
}

