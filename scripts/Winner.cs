﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Winner : MonoBehaviour
{
    GUIStyle style = new GUIStyle();
    public string winner_name;

    // Start is called before the first frame update
    void Awake()
    {
        style.alignment = TextAnchor.MiddleCenter;
        winner_name = GwLib.winner_name;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI () 
    {    
        GUI.backgroundColor = Color.yellow * Color.grey;
        GUI.Box (new Rect (0.18f * Screen.width,0.26f * Screen.height,620,250),
         this.winner_name + " buried every players, and become the GW5 champion !", this.style);
    }
}
